// This program was written by J�rgen Hoffmann in the year 2009
// and compiled under Borland C++ Version 3.1
// using the Small model and the "Compile via assembler" option
//
#include <dos.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <conio.h>

#define MAXBUFS     8  	                /* maximum number of Ethernet buffers */
#define BPMASK      7
#define BUFSIZE     1520

#define DOS         0x21
#define GETVECT     0x35

#define INT_FIRST 0x60
#define INT_LAST  0x80
#define PD_DRIVER_INFO	0x1ff
#define PD_ACCESS 	0x200
#define PD_RELEASE	0x300
#define PD_SEND 	0x400
#define PD_GET_ADDRESS	0x600
#define PD_RESET        0x700
#define PD_SET_MODE	0x1400
#define CARRY 		1	        /* carry bit in flags register */

#if defined __TINY__
#define MEMORY_MODEL	"ti"
#elif defined __SMALL__
#define MEMORY_MODEL	"sm"
#elif defined __MEDIUM__
#define MEMORY_MODEL	"me"
#elif defined __COMPACT__
#define MEMORY_MODEL	"co"
#elif defined __LARGE__
#define MEMORY_MODEL	"la"
#else
#define MEMORY_MODEL	"??"
#endif

typedef unsigned short word;            /* 16 bits */
typedef unsigned char  byte;            /*  8 bits */

char *version  = "V1.0";

unsigned long ip_addr;
unsigned long net_mask;
unsigned long net_addr;
byte  brief       = 1;
byte  hostmode    = 0;
byte  no_www      = 0;
byte  capabilities;
word  int_first   = INT_FIRST;
word  int_last    = INT_LAST;
word  pkt_interrupt;
word  pkt_type    = 0XFFFF;                /* any type */
word  pkt_handle;
byte  eth_addr[6] ;
char *pkt_line    = "PKT DRVR";
byte  old_driver  = 0;
byte  write_file  = 0;
FILE *outfil;
char  filnam[82]  = ".\\ETHWATCH.DMP";

typedef struct {
  word          etype;
  byte          VersHL;
  byte          Tos;
  word          TLen;
  word          id;
  word          FlgOff;
  byte          ttl;
  byte          prot;
  word          ChsHd;
  unsigned long srcaddr;
  unsigned long dstaddr;
  } ip_header;

typedef struct {
  word          srcport;
  word          dstport;
  } sub_header;

ip_header  *iph;
sub_header *shd;

typedef struct {
  word  pkt_num;
  word  pkt_len;
  byte  pkt_data[BUFSIZE];
  } pkt_buffer;

word  aux_seq           = 0;
word  total_lost        = 0;
word  pkt_last          = 0;
word  pkt_get           = 0;
word  pkt_put           = 0;
word  pkt_seq           = 0;
word  num_bufs          = MAXBUFS;
word  bpmask            = BPMASK;
pkt_buffer *pb;

// compiled with Borland C++  3.1
// with "Compile via assembler" option
// in Options | Compiler | Code generation ...
void far pkt_callback(void) {
  asm {
    pop  di; // compensate for compiler generated PUSH DI / POP DI
    push ds;                   // save driver's data segment
    mov  di,DGROUP;            // NOT in huge model !!!
    mov  ds,di;                // set C's data segment
    }
  disable();
  if(_AX) {
    pb[pkt_put & bpmask].pkt_len |= 0X8000;
    pkt_put++;
    }
  else {
    if(pb[pkt_put & bpmask].pkt_len || _CX >= BUFSIZE) {
      pkt_seq++;
      _ES = 0;
      _DI = 0;
      }
    else {
      pb[pkt_put & bpmask].pkt_len = _CX;
      pb[pkt_put & bpmask].pkt_num = pkt_seq;
      pkt_seq++;
      _ES = FP_SEG(pb[pkt_put & bpmask].pkt_data);
      _DI = FP_OFF(pb[pkt_put & bpmask].pkt_data);
      }
    }
  enable();
  asm {
    pop  ds; // restore driver's data segment
    push di; // compensate for compiler generated PUSH DI / POP DI
    }
  }

int pkt_init(void) {
  struct REGPACK regs;
  char far *temp;
  int pd_type;	                /* packet driver type */
  int pd_class;
  for (pkt_interrupt = int_first; pkt_interrupt <= int_last; ++pkt_interrupt ) {
    temp = (char far *)getvect( pkt_interrupt );
    if (!_fmemcmp( &(temp[3]), pkt_line, strlen( pkt_line ))) break;
    }
  if ( pkt_interrupt > int_last ) {
    fprintf(stderr,"NO PACKET DRIVER FOUND ");
    if(int_first==int_last) fprintf(stderr,"AT INTERRUPT 0x%02X\n",int_first);
    else fprintf(stderr,"IN RANGE 0x%02X .. 0x%02X\n",int_first,int_last);
    exit(2);
    }

  /* lets find out about the driver */
  regs.r_ax = PD_DRIVER_INFO;
  intr( pkt_interrupt, &regs );
  capabilities = regs.r_ax & 7;

  /* handle old versions, assume a class and just keep trying */
  if (regs.r_flags & CARRY ) {
    for ( pd_class = 0; pd_class < 19; ++pd_class ) {
      for (pd_type = 1; pd_type < 128; ++pd_type ) {
	regs.r_ax = PD_ACCESS | pd_class;  /* ETH, SLIP */
	regs.r_bx = pd_type;		     /* type */
	regs.r_dx = 0;		     /* if number */
	regs.r_cx = 0;                     // sizeof( pkt_type);
	regs.r_ds = FP_SEG( &pkt_type );
	regs.r_si = FP_OFF( &pkt_type );
	regs.r_es = FP_SEG( pkt_callback );
	regs.r_di = FP_OFF( pkt_callback );
	intr( pkt_interrupt, &regs );
	if ( ! (regs.r_flags & CARRY) ) break;
	}

      if (pd_type < 128 ) {
	/* get ethernet address */
	regs.r_ax = PD_GET_ADDRESS;
	regs.r_bx = regs.r_ax;	/* handle */
	regs.r_es = FP_SEG( eth_addr );
	regs.r_di = FP_OFF( eth_addr );
	regs.r_cx = sizeof( eth_addr );
	intr( pkt_interrupt, &regs );
	/* we have found a working type, so kill it */
	regs.r_ax = PD_RELEASE;
	intr( pkt_interrupt, &regs );
	old_driver++;
	fprintf(stderr,"Found OLD type of packet driver\n");
	break;
	}

      if ((pd_type == 128 ) && (pd_class > 18)) {
	fprintf(stderr,"ERROR initializing packet driver\n\r");
	return( 1 );
	}
      }
    }
  else {
    pd_type = regs.r_dx;
    pd_class = regs.r_cx >> 8;
    /* get ethernet address */
    regs.r_ax = PD_GET_ADDRESS;
    regs.r_bx = 0;
    regs.r_es = FP_SEG( eth_addr );
    regs.r_di = FP_OFF( eth_addr );
    regs.r_cx = sizeof( eth_addr );
    intr( pkt_interrupt, &regs );
    fprintf(stderr,"Found packet driver with basic ");
    switch (capabilities) {
      case 2:
      case 6: fprintf(stderr,"and extended ");
	      if(capabilities==2) break;
      case 5: fprintf(stderr,"and high-performance ");
      }
    fprintf(stderr,"functions\n");
    }
  fprintf(stderr,"Vector=%02X class=%d type=%d ethernet address=%02X:%02X:%02X:%02X:%02X:%02X\n",
	    pkt_interrupt,pd_class,pd_type,eth_addr[0],eth_addr[1],
	    eth_addr[2],eth_addr[3],eth_addr[4],eth_addr[5]);

  regs.r_ax = PD_ACCESS | pd_class;
  regs.r_bx = pd_type;                // any type
  regs.r_dx = 0;		      // if number
  regs.r_cx = 0;                      // sizeof( pkt_type );
  regs.r_ds = FP_SEG( &pkt_type );
  regs.r_si = FP_OFF( &pkt_type );
  regs.r_es = FP_SEG( pkt_callback );
  regs.r_di = FP_OFF( pkt_callback );
  intr( pkt_interrupt, &regs );
  if(regs.r_flags & CARRY) {
    fprintf(stderr,"ERROR #%02X accessing packet driver\n",(regs.r_dx>>8));
    return( 1 );
    }
  pkt_handle = regs.r_ax;
  return( 0 );
  }

void pkt_setmode(int mode) {
  struct REGPACK regs;
  // get ethernet address
  regs.r_ax = PD_SET_MODE;
  regs.r_bx = pkt_handle;
  regs.r_cx = mode;
  intr( pkt_interrupt, &regs );
  if(regs.r_flags & CARRY) fprintf(stderr,"WARNING cannot set driver into mode: %d\n",mode);
  }

void pkt_release(void) {
  struct REGPACK regs;
  int error;
  regs.r_ax = PD_RELEASE;
  regs.r_bx = pkt_handle;
  intr( pkt_interrupt, &regs );
  if (regs.r_flags & CARRY ) fprintf(stderr,"ERROR releasing packet driver\n");
  return;
  }

int pkt_send( char *buffer, int length ) {
  struct REGPACK regs;
  int retries;

  retries = 5;
  while (retries--) {
    regs.r_ax = PD_SEND;
    regs.r_ds = FP_SEG( buffer );
    regs.r_si = FP_OFF( buffer );
    regs.r_cx = length;
    intr( pkt_interrupt, &regs );
    if ( regs.r_flags & CARRY ) continue;
    return( 0 );
    }
  return( 1 );
  }


//---------------------------------------------------------------

char *ipad2asc(unsigned long ip) {
  static char tmp[18];
  byte *p;
  p = (byte *)&ip;
  sprintf(tmp,"%d.%d.%d.%d",p[0],p[1],p[2],p[3]);
  return(tmp);
  }

word htoi(char *p, int count) {
  int i;
  for(i=0; *p && count; p++,count--) {
    if(!isxdigit(*p)) break;
    i <<= 4;
    if(isdigit(*p)) i |= (*p&0x0F);
    else i |= ((*p&0X0F) + 9);
    }
  return(i);
  }

word swap(word arg) {
  return((arg & 0xFF00) >> 8) | ((arg & 0xFF) << 8);
  }

void set_buffer_size(char *p) {
  word i,j;
  i=atoi(p);
  if(i) {
    if((i>3)&&(i<6)) {
      for(j=1; i; i--) j <<= 1;
      num_bufs = j;
      bpmask = j - 1;
      }
    }
  }

//---------------------------------------------------------------

void seq_check(word pkt_num) {
  int i;
  if(pkt_num>(++pkt_last)) {
//  printf("%6d  %-60s\n\n",pkt_num-pkt_last,"packet(s) lost");
    total_lost += pkt_num-pkt_last;
    }
  pkt_last = pkt_num;
  }

int filter_check(pkt_buffer *pb) {
  int f;
  iph = (ip_header  *)&pb->pkt_data[12];
  shd = (sub_header *)&pb->pkt_data[14+(iph->VersHL&0XF)*4];
  if(iph->etype!=0X0008) return(0); // no ethernet_II IP frame
  f = 0;
  if((iph->srcaddr&net_mask)==net_addr) f |= 1;
  if((iph->dstaddr&net_mask)==net_addr) f |= 2;
  if((f!=1)&&(f!=2)) return(0);
  if(hostmode) {
    if(f==1) {
      if(iph->srcaddr!=ip_addr) return(0);
      }
    else
      if(iph->dstaddr!=ip_addr) return(0);
    }
  if(no_www) if(iph->prot==6) {
    if((shd->srcport==0X5000)||(shd->dstport==0X5000)) return(0);
    }
  return(f);
  }

void process_packet(pkt_buffer *pb) {
  int f,pl;
  word port;
  seq_check(pb->pkt_num);
  if((f=filter_check(pb))>0) {
    pl = pb->pkt_len & 0X7FFF;
    if(brief) {
      if(stdout->flags&_F_TERM) fprintf(stderr,"\r%70s\r","");
      if(f==1) {
	port = swap(shd->dstport);
	printf("To   %-15s ",ipad2asc(iph->dstaddr));
	}
      else {
	port = swap(shd->srcport);
	printf("From %-15s ",ipad2asc(iph->srcaddr));
	}
      switch(iph->prot) {
	  case  1: printf("ICMP");                               break;
	  case  6: printf("TCP port %-5u",port);
		   if(port==80) printf(" (WWW)");                break;
	  case 17: printf("UDP port %-5u",port);
		   if(port==53) printf(" (DNS)");                break;
	  default: printf("Unknown sub-protocol: %d",iph->prot); break;
	  }
      printf("\n");
      }
    if(outfil) {
      fwrite(&aux_seq,sizeof(byte),2,outfil);
      fwrite(&pl,sizeof(byte),2,outfil);
      fwrite(pb->pkt_data,sizeof(byte),pl,outfil);
      }
    aux_seq++;
    }
  }

int get_argument(char *p) {
  union {
    byte          b[5];
    unsigned long ul;
    } tmp;
  byte *q;
  char lim[5] = ".../", *p1;
  unsigned long ltmp;
  int i,j,k;
  q = &tmp.b[0];
  for(j=0; j<5; j++) {
    for(i=0,p1=p;isdigit(*p);p++,i++) { }
    if((i<1)||(i>3)||(*p!=lim[j])) return(1);
    if((k=atoi(p1))>255) return(2);
    *q++ = k;
    p++;
    }
  i = tmp.b[4];
  if(i>30) return(3);
  ip_addr  = tmp.ul;
  tmp.ul   = 0L;
  q = &tmp.b[0];
  for(j=0,*q=0; j<4&&i; j++,q++) for(k=0X80; k&&i; k>>=1, i--) *q |= k;
  net_mask = tmp.ul;
  net_addr = ip_addr & net_mask;
  return(0);
  }

void help_text(void) {
  printf("\n\n\n");
  printf("  ETHWATCH %s%s by J�rgen Hoffmann (2009) j_hoff@hrz1.hrz.tu-darmstadt.de\n\n",version,MEMORY_MODEL);
  printf("  usage: ethwatch [ options ] <ip-address>/<mask-length>\n\n");
  printf("         <ip-address>  may denote a single host  or an entire net.\n");
  printf("         <mask-length> is the number of  \"1\"-bits  in the netmask.\n");
  printf("         example:      host: 192.168.5.25/24   net: 192.168.5.0/24\n\n");
  printf("  valid options:\n");
  printf("    /I<int> define packet driver interrupt  (default: automatic)\n");
  printf("    /F[fil] write data to file \"fil\" (default: .\\ETHWATCH.DMP)\n");
  printf("            file format is compatible with ETHVIEW and ETHSHOW\n");
  printf("    /Q      don't show headers while capturing\n");
  printf("    /W      ignore traffic to or from TCP port 80 (WWW)\n");
  printf("    /Z<n>   allocate 16(n=4) or 32(n=5) packet buffers (default: 8)\n");
  printf("    /H /?   print this help text\n\n");
  }

void main(int argc, char *argv[]) {
  int  i;
  char *p;

  for(i = 1; i < argc; i++) {
    if(*argv[i]=='-' || *argv[i]=='/') {
      p = argv[i];
      switch(toupper(p[1])) {
	case 'I': if(isdigit(p[2])) {
		    if(toupper(p[3])=='X') int_first = htoi(&p[4],2);
		    else int_first = atoi(&p[2]);
		    int_last = int_first;
		    }                                               break;
	case 'F': if(p[2]) strncpy(filnam,&p[2],80); write_file++;  break;
	case 'W': no_www = 1;                                       break;
	case 'Q': brief = 0;                                        break;
	case 'Z': set_buffer_size(&p[2]);                           break;
	case 'H':
	case '?': help_text(); exit(1);
	} /* switch */
      } /* if */
    else {
      if(get_argument(argv[i])) {
	fprintf(stderr,"ERROR: bad address specification\n");
	exit(2);
	}
      }
    }
  if(!net_mask) {
    fprintf(stderr,"ERROR: no address specified\n");
    exit(2);
    }
  pkt_init();
  pkt_setmode(6);
  hostmode = (ip_addr!=net_addr);
  fprintf(stderr,"Filtering packets from %s %s to internet and vice versa\n",
		(hostmode?"host":"net"),ipad2asc(ip_addr));
  if(no_www) fprintf(stderr,"Ignoring traffic to or from TCP port 80 (WWW)\n");
  pb=calloc(num_bufs,sizeof(pkt_buffer));
  if(pb==NULL) {
    fprintf(stderr,"ERROR: cannot allocate enough buffer space\n");
    exit(2);
    }
  else if(num_bufs>MAXBUFS) fprintf(stderr,"Allocated %d packet buffers\n",num_bufs);
  if(write_file) {
    if(NULL==(outfil=fopen(filnam,"wb")))
      fprintf(stderr,"ERROR: cannot create file: %s\n",filnam);
      else fprintf(stderr,"Writing data to file: %s\n",filnam);
    }
  else fprintf(stderr,"NOT capturing data to file! (/F switch not set)\n");
  fprintf(stderr,"\n");
  if(!stdout->flags&_F_TERM) brief = 1;

  do {
    fprintf(stderr," %d/%d packets so far",aux_seq,pkt_seq);
    if(total_lost) fprintf(stderr," (%d%% lost)",(total_lost*100)/(pkt_seq));
    fprintf(stderr,"   Press any key to terminate ...\r");

    if(pb[pkt_get & bpmask].pkt_len&0x8000) {
      process_packet(&pb[pkt_get & bpmask]);
      pb[pkt_get & bpmask].pkt_len = 0;
      pkt_get++;
      if(pkt_get >= pkt_put) {
	pkt_get &= bpmask;
	pkt_put &= bpmask;
	}
      }
    else seq_check(pkt_seq);
    } while(!kbhit());
  getch();
  fprintf(stderr,"%-75s\n","");

  if(outfil) fclose(outfil);
  pkt_setmode(3);
  pkt_release();
  }



