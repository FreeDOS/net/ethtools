# ETHTOOLS

A set of diagnostic tools for ethernet networks. Built using the WatTCP stack. Includes ETHWHAT, WWWATCH, ETHSEND, ETHDUMP, ETHWATCH, ETHSHOW,

# Contributing

**Would you like to contribute to FreeDOS?** The programs listed here are a great place to start. Most of these do not have a maintainer anymore and need your help to make them better. Here's how to get started:

* __Maintainers__: Let us know if you'd like to take on one of these programs, and we can provide access to the source code repository here. Please use the FreeDOS package structure when you make new releases, including all executables, source code, and metadata.

* __New developers__: We can extend access for you to track issues here.

* __Translators__: Please submit to the [FD-NLS Project](https://github.com/shidel/fd-nls).

_*Make sure to check all source code licenses, especially for any code you might reuse from other projects to improve these programs. Note that not all open source licenses are the same or compatible with one another. (For example, you cannot reuse code covered under the GNU GPL in a program that uses the BSD license.)_

## ETHTOOLS.LSM

<table>
<tr><td>title</td><td>ETHTOOLS</td></tr>
<tr><td>version</td><td>8feb2010 (rev A)</td></tr>
<tr><td>entered&nbsp;date</td><td>2013-02-01</td></tr>
<tr><td>description</td><td>Diagnostic tools for ethernet networks</td></tr>
<tr><td>keywords</td><td>ethtools</td></tr>
<tr><td>author</td><td>Joergen Hoffmann</td></tr>
<tr><td>original&nbsp;site</td><td>http://www.bttr-software.de/products/jhoffmann/</td></tr>
<tr><td>platforms</td><td>DOS</td></tr>
<tr><td>copying&nbsp;policy</td><td>EUPL v1.1</td></tr>
<tr><td>summary</td><td>A set of diagnostic tools for ethernet networks. Built using the WatTCP stack. Includes ETHWHAT, WWWATCH, ETHSEND, ETHDUMP, ETHWATCH, ETHSHOW,</td></tr>
</table>
